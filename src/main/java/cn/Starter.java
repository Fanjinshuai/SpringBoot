package cn;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;



//springboot核心注解
//@SpringBootConfiguration
//@EnableAutoConfiguration
//@ComponentScan
@SpringBootApplication
//生成指定接口的代理对象
@MapperScan("cn.springboot.mapper")
//开启缓存
@EnableCaching
//开启定时器
@EnableScheduling
public class Starter {

    static Logger logger = LoggerFactory.getLogger(Starter.class);
    public static void main(String[] args) {
        logger.info("项目开始启动！");
        SpringApplication.run(Starter.class,args);
        logger.info("项目已启动！");
    }
}
