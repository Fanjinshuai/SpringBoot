package cn.springboot.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student implements Serializable {

    private String sno;

    private String sname;

    private String ssex;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sbirthday;

    private String classZz;

}
