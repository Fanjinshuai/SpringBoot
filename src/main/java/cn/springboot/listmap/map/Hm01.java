package cn.springboot.listmap.map;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Hm01 {

    public static void main(String[] args) {

//        String s1 = "a";
//        Integer s2 = 97;
//        System.out.println(s1.hashCode());
//        System.out.println(s2.hashCode());
//        System.out.println(s1 == s2.toString());
//        System.out.println(s1.equals(s2));
//        System.out.println(s1.hashCode() == s2.hashCode());
        LinkedHashMap map = new LinkedHashMap(16, 0.75f, true);
        map.put("a", "a");
        map.put("b", "b");
        map.put("c", "c");
        map.put("a", "aa");
        map.put(97, "97");

        map.get("b");
        map.forEach((k, v) -> {
            System.out.println(k + "+" + v);
        });

        Map map1 = new HashMap(16);
        map.put("123", 123);
        Map map2 = new Hashtable(16);
        Map map3 = new ConcurrentHashMap(16);

    }
}
