package cn.springboot.listmap.set;


import java.util.HashSet;
import java.util.Set;

public class Test {
    public static void main(String[] args) {
        Set set = new HashSet<>(16);
        User user = new User(100L, "zhangsan");
        User user0 = new User(100L, "zhangsan");
        User user1 = new User(200L, "zhangsan2");
        User user2 = new User(300L, "zhangsan3");

        set.add(user);
        set.add(user0);
        set.add(user1);
        set.add(user2);

        System.out.println(user.hashCode());
        System.out.println(user0.hashCode());

        System.out.println(user.equals(user0));
        System.out.println(user == user0);

        set.forEach(s -> {
            System.out.println(s.toString());
        });

    }

}
