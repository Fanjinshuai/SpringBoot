package cn.springboot.listmap.array;

import java.util.ArrayList;
import java.util.List;

public class Array {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4};
        List<List<Integer>> list = new ArrayList<>();
        List<Integer> list1;
        for (int i : arr) {
            list1 = new ArrayList<>();
            for (int j = 0; j < i; j++) {
                list1.add(arr[j]);
            }
            list.add(list1);
        }
        list.forEach((s -> {
            System.out.println(s);
        }));
    }
}
