package cn.springboot.listmap.array;

import java.util.ArrayList;
import java.util.List;

public class ArrayLists {

    public static void main(String[] args) {
        List list = new ArrayList(10);
        list.add("lisi");
        list.add(1,"zhangsan");
        list.forEach(l->{
            System.out.println(l );
        });
    }
}
