package cn.springboot.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response<T> implements Serializable {

    private String message;
    private Integer code;
    private T data;

    /**
     * 返回成功
     *
     * @param data
     * @param message
     * @param <T>
     * @return
     */
    public static <T> Response<T> succseeResponse(T data, String message) {
        Response<T> result = succseeResponse();
        result.setData(data);
        result.setMessage(message);
        return result;
    }

    public static <T> Response<T> succseeResponse(T data) {
        Response<T> result = succseeResponse();
        result.setData(data);
        return result;
    }

    public static Response succseeResponse() {
        Response result = new Response();
        result.setCode(0);
        result.setMessage("操作成功！");
        return result;
    }

    /**
     * 返回失败
     */
    public static <T> Response<T> failResponse(T data, String message) {
        Response<T> result = failResponse();
        result.setData(data);
        result.setMessage(message);
        return result;
    }

    public static <T> Response<T> failResponse(T data) {
        Response<T> result = failResponse();
        result.setData(data);
        return result;
    }

    public static Response failResponse() {
        Response result = new Response();
        result.setCode(1);
        result.setMessage("操作失败！");
        return result;
    }
}
