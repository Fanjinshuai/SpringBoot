package cn.springboot.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class RedisUtils {

    //获取redis模板
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public void setString(String key, String value) {
        setString(key, value, 60 * 60 * 24L);
    }

    public void setString(String key, String value, Long timeOut) {
        stringRedisTemplate.opsForValue().set(key, value);
        if (timeOut != null) {
            stringRedisTemplate.expire(key, timeOut, TimeUnit.SECONDS);
        }
    }

    public String getString(String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }

    public Long getTtl(String key) {
        return stringRedisTemplate.opsForValue().getOperations().getExpire(key);
    }

    public Boolean setNx(String key) {
        return setNx(key, 20L);
    }

    public Boolean setNx(String key, Long outTime) {
        return stringRedisTemplate.opsForValue().setIfAbsent(key, key, outTime, TimeUnit.SECONDS);
    }

}
