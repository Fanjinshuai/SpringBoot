package cn.springboot.util;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptResover {

    /**
     * 处理所有异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Response exceptionHandler(Exception e) {
        return Response.failResponse(null, e.getMessage());
    }

    ///**
    //	 * 处理自定义的业务异常
    //	 * @param req
    //	 * @param e
    //	 * @return
    //	 */
    //    @ExceptionHandler(value = BizException.class)
    //    @ResponseBody
    //	public  ResultBody bizExceptionHandler(HttpServletRequest req, BizException e){
    //    	logger.error("发生业务异常！原因是：{}",e.getErrorMsg());
    //    	return ResultBody.error(e.getErrorCode(),e.getErrorMsg());
    //    }
    //
    //	/**
    //	 * 处理空指针的异常
    //	 * @param req
    //	 * @param e
    //	 * @return
    //	 */
    //	@ExceptionHandler(value =NullPointerException.class)
    //	@ResponseBody
    //	public ResultBody exceptionHandler(HttpServletRequest req, NullPointerException e){
    //		logger.error("发生空指针异常！原因是:",e);
    //		return ResultBody.error(CommonEnum.BODY_NOT_MATCH);
    //	}

}
