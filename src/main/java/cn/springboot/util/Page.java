package cn.springboot.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Page extends LinkedHashMap<String, Object> {

    private static final long serialVersionUID = 1L;
    private int num = 1;
    private int size = 20;

    public Page(Map<String,Object> map){

        if (map.get("pageNum") != null && map.get("pageNum") != "") {
            this.num = Integer.parseInt(map.get("pageNum").toString());
        }

        if (map.get("pageSize") != null && map.get("pageSize") != "") {
            this.size = Integer.parseInt(map.get("pageSize").toString());
        }
    }



}
