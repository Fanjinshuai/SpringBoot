package cn.springboot.mapper;


import cn.springboot.entity.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface StudentMapper {

    int addStudent(Student student);

    int addStudentList(@Param("studentList") List<Student> studentList);

    int updateById(Student student);

    Student selectById(@Param("id") String id);

    List<Student> selectByStudent(Student student);

    List<Student> selectPage(Map<String, Object> map);


}
