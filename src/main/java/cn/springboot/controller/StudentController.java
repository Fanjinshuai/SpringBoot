package cn.springboot.controller;
import cn.springboot.entity.Student;
import cn.springboot.service.StudentService;
import cn.springboot.util.Page;
import cn.springboot.util.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/student")
@Api(value = "student用户模块")
public class StudentController {

    @Autowired
    private StudentService studentService;

    /**
     * @description: 新增用户
     * @author: fanjs
     * @create: 2021/9/28 15:28
     */
    @PostMapping("/addStudent")
    @ApiOperation(value = "新增用户")
    public Response addStudent(@RequestBody Student student) {
        return studentService.addStudent(student) > 0 ?
                Response.succseeResponse() :
                Response.failResponse();
    }

    /**
     * @description: 批量新增
     * @author: fanjs
     * @create: 2021/9/28 15:45
     */
    @PostMapping("/addStudentList")
    @ApiOperation(value = "批量新增")
    public Response addStudentList(@RequestBody List<Student> studentList) {
        return studentService.addStudentList(studentList) > 0 ?
                Response.succseeResponse() :
                Response.failResponse();
    }

    /**
     * 根据id更新用户
     * @param student
     * @return
     */
    @PostMapping("/updateById")
    @ApiOperation(value = "根据id更新用户")
    public Response updateById(@RequestBody Student student) {
        return studentService.updateById(student) > 0 ?
                Response.succseeResponse() :
                Response.failResponse();
    }

    /**
     * @description: 根据id查询
     * @author: fanjs
     * @create: 2021/9/28 14:07
     */
    @PostMapping("/selectById")
    @ApiOperation(value = "根据id查询")
    public Response selectById(@RequestBody Student student) {
        System.out.println("根据id查询用户，热部署！");
        return Response.succseeResponse(studentService.selectById(student.getSno()));
    }

    /**
     * @description: 多条件查询
     * @author: fanjs
     * @create: 2021/9/28 14:08
     */
    @PostMapping("/selectByStudent")
    @ApiOperation(value = "多条件查询")
    public Response selectByStudent(@RequestBody Student student) {
        return Response.succseeResponse(studentService.selectByStudent(student));
    }

    /**
     * @description: 查询分页
     * @author: fanjs
     * @create: 2021/9/28 17:01
     */
    @PostMapping("/selectPage")
    @ApiOperation(value = "查询分页")
    public Response selectPage(@RequestBody Map<String, Object> map) {
        Page query = new Page(map);
        return Response.succseeResponse(studentService.selectPage(query, map));
    }


}

