package cn.springboot.zting.xianliu.aoputil;

import com.google.common.util.concurrent.RateLimiter;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class XianLiuAop {

    //令牌桶
    private RateLimiter rateLimiter = RateLimiter.create(10.0);

    @Around(value = "@annotation(cn.springboot.zting.xianliu.aoputil.XianLiu)")
    public Object around(ProceedingJoinPoint joinPoint) {
        try {
            System.out.println("方法执行前");
            Object proceed = joinPoint.proceed();
            System.out.println(proceed);
            System.out.println("方法执行后");
            return proceed;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

}
