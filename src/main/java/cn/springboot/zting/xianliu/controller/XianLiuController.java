package cn.springboot.zting.xianliu.controller;

import cn.springboot.zting.xianliu.aoputil.XianLiu;
import com.google.common.util.concurrent.RateLimiter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class XianLiuController {

    //令牌桶
    private RateLimiter rateLimiter = RateLimiter.create(1.0);

    @RequestMapping("/add")
    @XianLiu
    public String add() {
        boolean b = rateLimiter.tryAcquire();
        if (!b) {
            return "当前登录人数较多，请稍后重试！";
        }
        return "my is add";
    }

    @RequestMapping("/update")
    public String update() {
        return "my is update";
    }

    @RequestMapping("/delete")
    public String delete() {
        return "my is delete";
    }


}
