package cn.springboot.zting.sort;

import java.util.Random;

public class SortService {

    //插入排序
    public void chaRuPaiXu(int number) {
        int[] arr = random(number);
        //排序核心代码  正序排序
        for (int i = 0; i < arr.length - 1; i++) {
            //跳过第一个元素，直接取第二个元素
            int arri = arr[i + 1];
            //比较前两个元素的大小
            int j = i;
            while (j >= 0) {
                if (arr[j + 1] < arr[j]) {
                    //第二个元素大于第一个元素，两个元素交换位置
                    arr[j + 1] = arr[j];
                    arr[j] = arri;
                    j--;
                } else {
                    break;
                }
            }
        }
        System.out.print("插入排序后：");
        for (int i : arr) {
            System.out.print(Integer.toString(i) + "、");
        }
    }

    //选择排序
    public void xuanZePaiXu(int number) {
        int[] arr = random(number);
        //排序核心代码  正序排序
        for (int i = 0; i < arr.length - 1; i++) {
            int arri = arr[i];
            int num = i;
            int j = i;
            while (j < arr.length-1) {
                if (arri > arr[j + 1]) {
                    arri = arr[j + 1];
                    num = j + 1;
                }
                j++;
            }
            arr[num] = arr[i];
            arr[i] = arri;
        }
        System.out.print("选择排序后：");
        for (int i : arr) {
            System.out.print(Integer.toString(i) + "、");
        }
    }

    //冒泡排序
    public void maoPaoPaiXu(int number){
        int[] arr = random(number);
        //冒泡排序核心代码
        for (int i = 0; i < arr.length; i++) {
            for (int i1 = 0; i1 < arr.length-1-i; i1++) {
                int arri = arr[i1];
                if(arr[i1]>arr[i1+1]) {
                    arr[i1] = arr[i1+1];
                    arr[i1 + 1] = arri;
                }
            }
        }
        System.out.print("冒泡排序后：");
        for (int i : arr) {
            System.out.print(Integer.toString(i) + "、");
        }
    }

    public int[] random(int number) {
        int[] arr = new int[number];
        for (int i = 0; i < number; i++) {
            arr[i] = new Random().nextInt(100);
        }
        System.out.print("排序前：");
        for (int i : arr) {
            System.out.print(Integer.toString(i) + "、");
        }
        System.out.println();
        return arr;
    }

}
