package cn.springboot.zting.sort;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class Start {


    public static void main(String[] args) {
        SortService sortService = new SortService();
        //插入排序
        //sortService.chaRuPaiXu(20);
        //选择排序
        //sortService.xuanZePaiXu(20);
        //冒泡排序
        sortService.maoPaoPaiXu(20);
    }
}
