package com.xn;

import java.util.Random;

public class Hello {

    //兔子数
    //有一对兔子，从出生后第3个月起每个月都生一对兔子，小兔子长到第三个月后每个月又生一对兔子，假如兔子都不死，问每个月的兔子对数为多少？
//    public static void main(String[] args) {
//        //计算16个月
//        int month = 6;
//        //计算繁殖了多少次
//        int num = month/3;
//        //初始兔子1对
//        int tuzi = 1;
//        for (int i = 1; i <= num; i++){
//            //每繁殖一次翻一倍
//            tuzi = tuzi * 2;
//            System.out.println("第"+i*3+"个月时，一共繁殖了"+i+"次，一共"+tuzi+"对兔子");
//        }
//    }

    //判断101-200之间有多少个素数，并输出所有素数。
//    public static void main(String[] args) {
//        for (int i = 101; i <= 200; i++) {
//            //看是否为素数
//            Boolean flag = true;
//            for (int j = 2; j < i; j++) {
//                if (i % j == 0) {
//                    flag = false;
//                }
//            }
//            if (flag) {
//                System.out.print(i + ",");
//            }
//        }
//    }

    //打印出所有的“水仙花数”，所谓“水仙花数”是指一个三位数，其各位数字立方和等于该数本身。例如：153是一个“水仙花数”，因为153=13＋53＋33。
//    public static void main(String[] args) {
//        for (int i = 100; i < 1000; i++) {
//            int a = i / 100;
//            int b = (i - (a * 100)) / 10;
//            int c = (i - (a * 100) - (b * 10));
//            if (a * a * a + b * b * b + c * c * c == i) {
//                System.out.println("水仙花数字" + i);
//            }
//        }
//    }

    //利用条件运算符的嵌套来完成此题：学习成绩>=90分的同学用A表示，60-89分之间的用B表示，60分以下的用C表示。
//    public static void main(String[] args) {
//        var list = List.of(59, 79, 89, 94, 82, 65, 87, 81, 54, 82, 38, 94, 76, 58, 84, 82, 86, 95, 18, 75, 64, 59, 84, 75, 49, 85, 51, 29, 84, 73, 84, 99);
//        List list1 = new ArrayList();
//        for (Integer l : list) {
//            if (l >= 90) {
//                list1.add("A");
//            } else if (l >= 60) {
//                list1.add("B");
//            } else {
//                list1.add("C");
//            }
//        }
//        list1.forEach(s -> System.out.print(s));
//    }

    //求s=a+aa+aaa+aaaa+aa…a的值，其中a是一个数字。例如2+22+222+2222+22222(此时共有5个数相加)，几个数相加有键盘控制。输出结果的形式如：2+22+222=246。
//    public static void main(String[] args) {
//        int count = 3;
//        int num = 2;
//        int begin = num;
//        System.out.print(num);
//        for (int i = 1; i < count; i++) {
//            num = num * 10 + 2;
//            begin += num;
//            System.out.print("+"+num);
//        }
//        System.out.println("="+begin);
//    }

    //一球从100米高度自由落下，每次落地后反跳回原高度的一半；n次落地经过路线总长度和下次反弹的高度。
//    public static void main(String[] args) {
//        double heigh = 100;
//        int count = 3;
//        double sum = heigh;
//        for (int i = 1; i < count; i++) {
//            heigh = heigh / 2;
//            sum += heigh*2;
//        }
//        System.out.println(count + "次落地经过路线总长度" + sum + "下次反弹的高度" + heigh / 2);
//    }

    //有1、2、3、4个数字，能组成多少个互不相同且无重复数字的三位数？都是多少？
//    public static void main(String[] args) {
//        int a = 1, b = 2, c = 3, d = 4;
//        int[] arr = new int[]{1, 2, 3, 4};
//        for (int i = 0; i < arr.length; i++) {
//            for (int j = 0; j < arr.length; j++) {
//                for (int k = 0; k < arr.length; k++) {
//                    if (arr[i] == arr[j] || arr[j] == arr[k] || arr[i] == arr[k]) {
//                        continue;
//                    }
//                    System.out.println(arr[i] * 100 + arr[j] * 10 + arr[k]);
//                }
//            }
//        }
//    }

    //输入三个整数x、y、z，请把这三个数由小到大输出。
//    public static void main(String[] args) {
//        int[] arr = new int[10];
//        Random r = new Random();
//        for (int i = 0; i < arr.length; i++) {
//            arr[i] = r.nextInt(100);
//        }
//        for (int i : arr) {
//            System.out.print(i + "++");
//        }
//        for (int a = 0; a < arr.length; a++) {
//            for (int b = 0; b < arr.length - a-1; b++) {
//                if (arr[b] > arr[b + 1]) {
//                    int begin = arr[b];
//                    arr[b] = arr[b + 1];
//                    arr[b + 1] = begin;
//                }
//            }
//        }
//        System.out.println();
//        for (int i : arr) {
//            System.out.print(i + "++");
//        }
//    }

    //输出九九乘法表。
//    public static void main(String[] args) {
//        for (int i = 1; i <= 9; i++) {
//            for (int j = i; j <= 9; j++) {
//                System.out.print(i + "*" + j + "=" + i * j+"  ");
//            }
//            System.out.println();
//        }
//    }

    //猴子吃桃问题：猴子第一天摘下若干个桃子，当即吃了一半，还不瘾，又多吃了一个第二天早上又将剩下的桃子吃掉一半，又多吃了一个。以后每天早上都吃了前一天剩下的一半零一个。
    // 到第10天早上想再吃时，见只剩下一个桃子了。求第一天共摘了多少。
//    public static void main(String[] args) {
//        int number = 1;
//        for (int i = 0; i < 9; i++) {
//            number = (number + 1) * 2;
//            System.out.println(number);
//        }
//        System.out.println(number);
//    }
//    /**
//     * 1534  767766 383382 191190 9594 4746 2322 1110 54 21 1
//     */

    //有5个人坐在一起，问第五个人多少岁？他说比第4个人大2岁。问第4个人岁数，他说比第3个人大2岁。问第三个人，又说比第2人大两岁。问第2个人，说比第一个人大两岁。最后问第一个人，他说是10岁。请问第五个人多大？
//    public static void main(String[] args) {
//        int age = 10;
//        for (int i = 0; i < 4; i++) {
//            age += 2;
//        }
//        System.out.println(age);
//    }

    //求0，1，2，3，4，5，6，7所能组成的8位奇数个数。
//    public static void main(String[] args) {
//        int num = 0;
//        for (int i = 10000000; i <= 77777777; i++) {
//            if ((i + "").contains("8") || (i + "").contains("9")) {
//                continue;
//            }
//            if (i % 2 != 0) {
//                num += 1;
//            }
//        }
//        System.out.println(num);
//        int i = 7 * 8 * 8 * 8 * 8 * 8 * 8 * 4;
//        System.out.println(i);
//    }

    //随机生成7个数（1—50的整数），每生成一个数，打印出该相应数量的*。
//    public static void main(String[] args) {
//        Random r = new Random();
//        for (int i = 0; i < 7; i++) {
//            int number = r.nextInt(100);
//            System.out.print(number);
//            for (int j = 0; j < number; j++) {
//                System.out.print("*");
//            }
//            System.out.println();
//        }
//    }
}
