package cn.springboot.zting.getTxt;

import cn.springboot.zting.sort.Start;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class TxtStart {
    public static void main(String[] args) {
        Start();
    }

    public static void Start() {
        InputStream resourceAsStream = TxtStart.class.getResourceAsStream("123.txt");

        Map map = new HashMap(16);
        BufferedReader in = new BufferedReader(new InputStreamReader(resourceAsStream));
        String s = null;
        try {
            while ((s = in.readLine()) != null) {
                if (!"".equals(s.trim())) {
                    String[] arr = s.split(",");
                    if (arr.length == 3) {
                        String name = arr[1];
                            map.put(name, name);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        map.forEach((key,value)->{
            System.out.println(key+"+"+value);
        });

    }
}
