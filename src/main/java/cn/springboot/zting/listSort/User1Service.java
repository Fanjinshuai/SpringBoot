package cn.springboot.zting.listSort;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class User1Service {
    public static void main(String[] args) {
        List<User> list = new ArrayList<>();
        User user = new User(001L, "zhangsan", 18, (byte) 1, 85, 76, 90);
        list.add(user);
        user = new User(002L, "lisi", 22, (byte) 0, 68, 90, 78);
        list.add(user);
        user = new User(003L, "wangwu", 24, (byte) 1, 80, 84, 81);
        list.add(user);
        user = new User(004L, "zhaoliu", 17, (byte) 1, 100, 90, 87);
        list.add(user);
        user = new User(005L, "qianqi", 21, (byte) 0, 89, 78, 88);
        list.add(user);

        Stream<User> stream = list.stream();
        Map<Long, String> collect = stream.collect(Collectors.toMap(User::getId, User::getName));
        collect.forEach((key, value) -> {
            System.out.println(key + ":" + value);
        });

        list.sort((a,b)->a.getYingyu().compareTo(b.getYingyu()));

        for (User user1 : list) {
            System.out.println(user1.toString());
        }

    }
}
