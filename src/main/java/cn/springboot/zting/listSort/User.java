package cn.springboot.zting.listSort;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private Long id;

    private String name;

    private Integer age;

    private Byte sex;

    private Integer yuwen;

    private Integer shuXue;

    private Integer yingyu;

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                ", yuwen=" + yuwen +
                ", shuXue=" + shuXue +
                ", yingyu=" + yingyu +
                '}';
    }
}
