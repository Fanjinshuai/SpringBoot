package cn.springboot.zting.redis.controller;

import cn.springboot.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Lock {

    @Autowired
    private RedisUtils redisUtils;

    /**
     * 定时执行，通知
     */
    //上一次开始执行时间点之后0秒再执行
    //@Scheduled(cron = "*/10 * * * * ?")
    public void sendMassage() {
        Long t = redisUtils.getTtl("message");
        if (t == null || t < 3L) {
            redisUtils.setString("message", "1", 10L);
            System.out.println("分布式下，保证线程执行的幂等性，多个jvm只执行一次！" + System.currentTimeMillis());
        }
    }

    //@Scheduled(cron = "*/1 * * * * ?")
    public void sendMessages() {
        //System.out.println("获取一次锁！"+ System.currentTimeMillis());
        if (redisUtils.setNx("setNx", 5L)) {
            System.out.println(System.currentTimeMillis()/1000 + "获取锁成功！");
        }


    }


}
