package cn.springboot.zting.redis.controller;

import cn.springboot.zting.redis.Utils.RedisLockUtils;

public class RedisLockService {

    private static final String LOCK_KEY = "xiao_xin_xin";

    public static void service() {
        //获取锁
        RedisLockUtils redisLockUtils = new RedisLockUtils();
        String lockValue = redisLockUtils.getLock(LOCK_KEY, 5000,5000);
        //获取锁失败
        if (lockValue == null) {
            System.out.println(Thread.currentThread().getName()+"获取锁失败了！");
            return;
        }
        //获取锁成功
        System.out.println("获取锁成功" + System.currentTimeMillis() + "+" + lockValue);
        //释放锁
        // 1采用删除key(宕机未删除会存在死锁)
        // 2添加时间为key设置有效期,自动失效
//        if (redisLockUtils.unLock(LOCK_KEY, lockValue)) {
//            //释放锁成功
//            System.out.println("释放锁成功" + System.currentTimeMillis() + "+" + lockValue);
//        }
    }


    public static void main(String[] args) {
        service();
    }
}
