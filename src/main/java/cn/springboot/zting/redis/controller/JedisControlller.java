package cn.springboot.zting.redis.controller;

import cn.springboot.zting.redis.Utils.JedisUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;

@RestController
@RequestMapping("/jedis")
public class JedisControlller {

    @PostMapping("/get")
    public String getJedis() {
        String key = "zhangsan";
        Jedis jedis = JedisUtils.getJedis();
        jedis.set(key, "xiaobaobei");
        jedis.expire(key, 5000);
        System.out.println("zhangsan已存入缓存！");
        String s = jedis.get(key);
        JedisUtils.close(jedis);
        return s;

    }
}
