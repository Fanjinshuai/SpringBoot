package cn.springboot.zting.redis.controller;

import cn.springboot.entity.Student;
import cn.springboot.service.StudentService;
import cn.springboot.util.Page;
import cn.springboot.util.RedisUtils;
import cn.springboot.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/redis")
public class RedisStudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private RedisUtils redisUtils;


    /**
     * @description: 根据id查询
     * @author: fanjs
     * @create: 2021/9/28 14:07
     */
    @PostMapping("/selectById")
    @Cacheable(cacheNames = "student", key = "'selectById'")
    public Response selectById(@RequestBody Student student) {
        System.out.println("根据id查询用户");
        return Response.succseeResponse(studentService.selectById(student.getSno()));
    }

    /**
     * @description: 多条件查询
     * @author: fanjs
     * @create: 2021/9/28 14:08
     */
    @PostMapping("/selectByStudent")
    public Response selectByStudent(@RequestBody Student student) {
        List<Student> list = studentService.selectByStudent(student);
        redisUtils.setString("selectByStudent", list.toString(), 10L);
        System.out.println(redisUtils.getString("selectByStudent"));
        return Response.succseeResponse(list);
    }

}

