package cn.springboot.zting.redis.Utils;

import redis.clients.jedis.Jedis;

import java.util.UUID;

public class RedisLockUtils {

    private static final int LOCK_SUCESS = 1;

    //获取锁
    public String getLock(String lockKey, int notLockTime,int unTime) {
        //获取jedis连接
        Jedis jedis = JedisUtils.getJedis();
        //计算尝试获取锁的超时时间
        Long endTime = System.currentTimeMillis() + notLockTime;
        //当前系统之间小于超时时间，没超时
        try {
            while (System.currentTimeMillis() < endTime) {
                //获取锁
                String lockValue = UUID.randomUUID().toString();
                //setNx返回1是，为创建成功，即获取锁成功
                if (jedis.setnx(lockKey, lockValue) == LOCK_SUCESS) {
                    //为锁设置过期时间避免死锁
                    jedis.expire(lockKey, unTime/1000);
                    return lockValue;
                    //退出循环
                } else {
                    Long ttl = jedis.ttl(lockKey);
                    System.out.println("线程还剩" + (endTime - System.currentTimeMillis()) + "ms超时");
                    System.out.println("锁" + lockKey + "还有" + ttl + "s过期！");
                }
                //没有获取到锁，且没有到期，继续获取锁
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    //释放锁
    public Boolean unLock(String lockKey, String lockValue) {
        //获取edis连接
        Jedis jedis = JedisUtils.getJedis();
        //判断是否是自己家的锁，由自己进行释放
        try {
            if (lockValue.equals(jedis.get(lockKey))) {
                return jedis.del(lockKey) > 0 ? true : false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return false;
    }
}
