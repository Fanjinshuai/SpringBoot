package cn.springboot.service;

import cn.springboot.entity.Student;
import cn.springboot.mapper.StudentMapper;
import cn.springboot.util.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class StudentService {

    @Autowired
    private StudentMapper studentMapper;

    public int addStudent(Student student) {
        return studentMapper.addStudent(student);
    }

    public int addStudentList(List<Student> studentList) {
        return studentMapper.addStudentList(studentList);
    }

    public int updateById(Student student) {
        return studentMapper.updateById(student);
    }

    public Student selectById(String id) {
        return studentMapper.selectById(id);
    }

    public List<Student> selectByStudent(Student student) {
        return studentMapper.selectByStudent(student);
    }

    public PageInfo<Student> selectPage(Page query, Map<String, Object> map) {
        PageHelper.startPage(query.getNum(), query.getSize());
        List<Student> list = studentMapper.selectPage(map);
        PageInfo<Student> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }


}
